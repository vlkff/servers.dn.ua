﻿<?php
// $Id: views-view-fields.tpl.php,v 1.6 2008/09/24 22:48:21 merlinofchaos Exp $
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

// getting addtocart link html-code
$ws_cart = unserialize($_COOKIE['ws_cart']);
if (array_key_exists($row->nid,$ws_cart['ws_cart'])) {
          $img = '<img src="'.path_to_theme().'/cart_remove.png'.'" title="'.t('Remove from cart').'">';
        }
        else {
          $img = '<img src="'.path_to_theme().'/cart_add.png'.'" title="'.t('Add to cart').'">';  
        }
        
$link = '<a class="next_link" href="'.url('cart/'.$row->nid, array('query' => array('destination' => $_GET['q'])  )).'">'.$img.'</a>';
?>
<div class="good">
	<div class="good-img">
	<?= $fields['field_image_fid']->content; ?>
	</div>
	<div class="good-content">
	<div class="good-title"><?=  $fields['title']->content;  ?></div>
        <div class="good-desc"><?=  $fields['teaser']->content;  ?></div>
        <div class="good-price"><?=  $fields['field_price_amount']->content;  ?></div>
	<div class="good-add-to-cart"><?= $link; ?></div>
	</div>
</div>
<div class="good-separator"></div>
