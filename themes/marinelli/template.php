<?php

//template for Marinelli Theme
//author: singalkuppe - www.signalkuppe.com




function marinelli_width($left, $right) {
  $width = "short";
  if (!$left) {
    $width = "long";
  }

  if (!$right) {
    $width = "long";
  }
  return $width;
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    array_shift($breadcrumb);
    return '<div class="path"><p><span>' . t('You are here') . '</span>' . implode(' / ', $breadcrumb) . '</p></div>';
  }
}

//overrides taxonomy term page function
function marinelli_taxonomy_term_page($tids, $result) {
  drupal_add_css(drupal_get_path('module', 'taxonomy') . '/taxonomy.css');

  $output = '';

  // Only display the description if we have a single term, to avoid clutter and confusion.
  if (count($tids) == 1) {
    $term = taxonomy_get_term($tids[0]);
    $description = $term->description;

    // Check that a description is set.
    if (!empty($description)) {
      $output .= '<div class="terminfo"><p>';
      $output .= filter_xss_admin($description);
      $output .= '</p></div>';
    }
  }

  $output .= taxonomy_render_nodes($result);

  return $output;
}

function marinelli_admin_page($blocks) {
  $stripe = 0;
  $container = array();

  foreach ($blocks as $block) {
    if ($block_output = theme('admin_block', $block)) {
      if (empty($block['position'])) {
        // perform automatic striping.
        $block['position'] = ++$stripe % 2 ? 'left' : 'right';
      }
      if (!isset($container[$block['position']])) {
        $container[$block['position']] = '';
      }
      $container[$block['position']] .= $block_output;
    }
  }

  $output = '<div class="admin clear-block">';
  $output .= '<div class="compact-link"><p>'; // use <p> for hide/show anchor
  if (system_admin_compact_mode()) {
    $output .= l(t('Show descriptions'), 'admin/compact/off', array('title' => t('Expand layout to include descriptions.')));
  } else {
    $output .= l(t('Hide descriptions'), 'admin/compact/on', array('title' => t('Compress layout by hiding descriptions.')));
  }
  $output .= '</p></div>';

  foreach ($container as $id => $data) {
    $output .= '<div class="' . $id . ' clear-block">';
    $output .= $data;
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}

function marinelli_admin_block_content($content) {
  if (!$content) {
    return '';
  }

  if (system_admin_compact_mode()) {
    $output = '<dl class="menu">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>'; // use definition list per compact mode
    }
    $output .= '</dl>';
  } else {
    $output = '<dl class="admin-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . $item['description'] . '</dd>';
    }
    $output .= '</dl>';
  }
  return $output;
}

function marinelli_system_admin_by_module($menu_items) { // admin by module page
  $stripe = 0;
  $output = '';
  $container = array('left' => '', 'right' => '');
  $flip = array('left' => 'right', 'right' => 'left');
  $position = 'left';

  // Iterate over all modules
  foreach ($menu_items as $module => $block) {
    list($description, $items) = $block;

    // Output links
    if (count($items)) {
      $block = array();
      $block['title'] = $module;
      $block['content'] = theme('item_list', $items);
      $block['description'] = t($description);

      if ($block_output = theme('admin_block', $block)) {
        if (!isset($block['position'])) {
          // Perform automatic striping.
          $block['position'] = $position;
          $position = $flip[$position];
        }
        $container[$block['position']] .= $block_output;
      }
    }
  }

  $output = '<div class="bymodule">';
  foreach ($container as $id => $data) {
    $output .= '<div class="' . $id . ' clear-block">';
    $output .= $data;
    $output .= '</div>';
  }
  $output .= '</div>';

  return $output;
}

function phptemplate_get_primary_links() {
  return menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
}

// retrieve custom theme settings


$preload = theme_get_setting('cssPreload'); // print the js file if we choose css image preload

if ($preload == '1') {

  drupal_add_js(drupal_get_path('theme', 'marinelli') . '/js/preloadCssImages.jQuery_v5.js'); // load the javascript
  drupal_add_js('$(document).ready(function(){
		
	$.preloadCssImages();
		
	});
	
	', 'inline');
}


$valore = theme_get_setting('menutype'); // if we choose dropdown

if ($valore == '1') {

  drupal_add_js(drupal_get_path('theme', 'marinelli') . '/js/jquery.hoverIntent.minified.js'); // load the javascript
  drupal_add_js(drupal_get_path('theme', 'marinelli') . '/js/marinellidropdown.js'); // load the javascript
  drupal_add_css(drupal_get_path('theme', 'marinelli') . '/dropdown.css'); // load the css
}

function marinelli_rbcurrency_correct_value($element) {
  if (!module_exists('currency')) {
    drupal_set_message(t('Please enable the Currency module, it is requires by the Theme'), 'error');
    return;
  }
  $rbcurrency_tax = variable_get('rbcurrency_tax', 1) / 100; //Делим на 100, потому что проценты.
  $rbcurrency_uah_cost = variable_get('rbcurrency_uah_cost', 1);
  $rbcurrency_uah_bn_cost = variable_get('rbcurrency_uah_bn_cost', 1);

  switch (rbcurrency_get_current()) {
    case 'usd':
      $val = $element['#item']['value'];
      $val = $val + ($val * $rbcurrency_tax);
      return $val;
      break;
    case 'uah':
      $val = $element['#item']['value'] * $rbcurrency_uah_cost;
      $val = $val + ($val * $rbcurrency_tax);
      return $val;
      break;
    case 'uah_bn':
      $val = $element['#item']['value'] * $rbcurrency_uah_bn_cost;
      $val = $val + ($val * $rbcurrency_tax);
      return $val;
      break;
  }
}

function marinelli_money_field($amount, $currency, $display_options) {
  $output = '';
  foreach (explode('|', $display_options) as $option) {
    switch ($option) {
      case 'a':
        // The amount.
        //$amount = str_replace(' ','', $amount); ;
        //$amount = number_format($amount, 2, ',', ' ');
        $output = rbcurrency_correct_value($amount);
        break;
      case 's':
//        // Currency symbol.
//        $currency_symbols = currency_api_get_symbols();
//        if (isset($currency_symbols[$currency])) {
//          $output = $currency_symbols[$currency];
//          break;
//        }
//        // Fall back to currency code.
      case 'c':
        // Currency code.
        // $output .= $currency;
        break;
      case '+':
        // Separator.
        // $output .= $separator;
        break;
    }
  }
  return $output;
}

//function marinelli_number_formatter_default($element) {
//
//  $value = marinelli_rbcurrency_correct_value($element);
//  $rbcurrency_current = rbcurrency_get_current();
//  $output = number_format($value, 2, '.', ',') . rbcurrency_current_keys_replace($rbcurrency_current);
//  //dsm($element);
//  $element['#node']->field_price['0']['currency'] = 'UAH';
//  return $output;
//}
