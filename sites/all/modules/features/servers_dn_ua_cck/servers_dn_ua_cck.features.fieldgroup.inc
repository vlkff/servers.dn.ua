<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function servers_dn_ua_cck_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_platform_hdd_slot
  $groups['platform-group_platform_hdd_slot'] = array(
    'group_type' => 'multigroup',
    'type_name' => 'platform',
    'group_name' => 'group_platform_hdd_slot',
    'label' => 'Слот под винты',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'teaser' => array(
          'format' => 'fieldset',
        ),
        'full' => array(
          'format' => 'fieldset',
        ),
        '4' => array(
          'format' => 'fieldset',
        ),
        '2' => array(
          'format' => 'fieldset',
        ),
        '3' => array(
          'format' => 'fieldset',
        ),
        'token' => array(
          'format' => 'fieldset',
        ),
        'description' => NULL,
      ),
      'multigroup' => array(
        'multiple' => 1,
      ),
    ),
    'weight' => '-2',
    'parent' => '',
    'fields' => array(
      '0' => 'field_hdd_slot_type',
      '1' => 'field_hdd_slot_amount',
    ),
    'depth' => 0,
    'parents' => array(
      '0' => '',
    ),
  );

  // Exported group: group_platform_max_ram
  $groups['platform-group_platform_max_ram'] = array(
    'group_type' => 'multigroup',
    'type_name' => 'platform',
    'group_name' => 'group_platform_max_ram',
    'label' => 'Ограничение по памяти',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'teaser' => array(
          'format' => 'fieldset',
        ),
        'full' => array(
          'format' => 'fieldset',
        ),
        '4' => array(
          'format' => 'fieldset',
        ),
        '2' => array(
          'format' => 'fieldset',
        ),
        '3' => array(
          'format' => 'fieldset',
        ),
        'token' => array(
          'format' => 'fieldset',
        ),
        'description' => NULL,
      ),
      'multigroup' => array(
        'multiple' => 1,
      ),
    ),
    'weight' => '7',
    'parent' => 'group_platform_ram',
    'fields' => array(
      '0' => 'field_platform_memory_type',
      '1' => 'field_platform_memory_max_size',
      '2' => 'field_platform_memory_max_module',
    ),
    'depth' => 1,
    'parents' => array(
      '0' => 'group_platform_ram',
    ),
  );

  // Exported group: group_platform_ram
  $groups['platform-group_platform_ram'] = array(
    'group_type' => 'standard',
    'type_name' => 'platform',
    'group_name' => 'group_platform_ram',
    'label' => 'RAM',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-3',
    'parent' => '',
    'fields' => array(
      '0' => 'field_platform_ram_slot_amount',
    ),
    'depth' => 0,
    'parents' => array(
      '0' => '',
    ),
  );

  // Exported group: group_raid_controller_ports
  $groups['raid_controller-group_raid_controller_ports'] = array(
    'group_type' => 'multigroup',
    'type_name' => 'raid_controller',
    'group_name' => 'group_raid_controller_ports',
    'label' => 'RAID порты',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'teaser' => array(
          'format' => 'fieldset',
        ),
        'full' => array(
          'format' => 'fieldset',
        ),
        '4' => array(
          'format' => 'fieldset',
        ),
        '2' => array(
          'format' => 'fieldset',
        ),
        '3' => array(
          'format' => 'fieldset',
        ),
        'token' => array(
          'format' => 'fieldset',
        ),
        'description' => NULL,
      ),
      'multigroup' => array(
        'multiple' => 1,
      ),
    ),
    'weight' => '0',
    'parent' => '',
    'fields' => array(
      '0' => 'field_raid_port_type',
      '1' => 'field_raid_port_amount',
    ),
    'depth' => 0,
    'parents' => array(
      '0' => '',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('RAID порты');
  t('RAM');
  t('Ограничение по памяти');
  t('Слот под винты');

  return $groups;
}
