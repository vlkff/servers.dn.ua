<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function servers_dn_ua_cck_imagecache_default_presets() {
  $items = array(
    'preview_250x250' => array(
      'presetname' => 'preview_250x250',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '250',
            'height' => '250',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'thumbnail_75x75' => array(
      'presetname' => 'thumbnail_75x75',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '200',
            'height' => '95',
            'upscale' => 1,
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function servers_dn_ua_cck_node_info() {
  $items = array(
    'cpu' => array(
      'name' => t('CPU'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'expansion_board' => array(
      'name' => t('Плата расширения'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'news' => array(
      'name' => t('Новость'),
      'module' => 'features',
      'description' => t('новости фирмы'),
      'has_title' => '1',
      'title_label' => t('Заголовок новости'),
      'has_body' => '1',
      'body_label' => t('Текст новости'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'os' => array(
      'name' => t('OS'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'platform' => array(
      'name' => t('Платформа'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'raid_controller' => array(
      'name' => t('RAID Контроллер'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'raid_options' => array(
      'name' => t('Опции RAID Контроллера'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'rails' => array(
      'name' => t('Рельсы'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'ram' => array(
      'name' => t('Модуль памяти'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'ups' => array(
      'name' => t('UPS'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'has_body' => '1',
      'body_label' => t('Текст'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
