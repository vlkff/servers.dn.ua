<?php

/**
 * @file
 *   Implements a base class for configurator item
 */

/*
 * ToDo: add caching here
 */
function configurator_get_form_items_classes () {
  $form_items_classes = array();
  $classes = get_declared_classes();
  foreach ($classes as $class) {
    // replace to is parent of
    if (get_parent_class($class) == 'ConfiguratorFormItem') {
      $form_items_classes[] = $class;
    }
  }
  return $form_items_classes;
}

class ConfiguratorFormItem {

  // interface
  public static function getPane($form, $form_state){

  }

  public static function getSubmittedItems($basename, $form, &$form_state) {

    $return = array();
    if (!empty($form_state['values'][$basename.'_nid'])) {
      $return[] = array (
        'nid' => $form_state['values'][$basename.'_nid'],
        'quantity' => 1,
      );
    }

    if (!empty($form_state['values'][$basename.'_quantity'])) {
      $return[key($return)]['quantity'] = $form_state['values'][$basename.'_quantity'];
    }
    return $return;


  }


  /**
   * Добывает массив по типу содержимого.
   *  'nid' => 'title'
   * $nodetype принимает тип материала
  */
  static protected function getPaneSelectOptions ($nodetype, $nid=0) {

    $result = db_query('
      SELECT node.`nid` AS nid, node.`title` AS title,
        price.`field_price_amount` AS amount,
        price.`field_price_currency` AS currency
      FROM {node} node LEFT JOIN {content_field_price} price
      ON node.`nid` = price.`nid`
      WHERE node.`type` = "%s" AND node.`status` = %d %s
      ORDER BY price.`field_price_amount`',
        $nodetype, 1, $nid > 0 ? "AND node.`nid` = $nid" : '');
    $arr = array();
    while ($node = db_fetch_object($result)) {
      $arr[$node->nid] = $node->title
            . ' ('
            . self::formatPrice(array('amount' => $node->amount,
                'currency' => $node->currency))
            . ')';
    }
    return $arr;
  }

  static protected function getPrices ($nids) {
    $nids = implode(',', $nids);
    $prices = array();
    $result = db_query('
      SELECT p.`nid` AS nid, p.`field_price_amount` AS price, n.`type` AS `type`
      FROM {content_field_price} p JOIN {node} n
      ON p.`nid` = n.`nid`
      WHERE p.`nid` IN (%s)', $nids
    );
    while ($row = db_fetch_object($result)) {
        $prices[$row->nid] = $row->price;
        if ($row->type == 'os') {
          $os_result = db_query('
            SELECT `field_price_above_amount` AS price
            FROM {content_type_os}
            WHERE `nid` = "%s"', $row->nid);
          $prices[$row->nid] = array(
              $prices[$row->nid],
              db_result($os_result)
          );
        }
    }
    return $prices;
  }

  static protected function calculatePrice ($items, $prices) {
    $price = 0;
    foreach ($items as $nid => $quantity) {
      if (!is_array($prices[$nid])) {
        $price += $prices[$nid] * $quantity;
      } elseif ($quantity > 0) {
        $price += $prices[$nid][0];
        $price += $prices[$nid][1] * ($quantity-1);
      }
      
    }
    return $price;
  }

  static public function getItemsPrice ($items) {
    $nids = array_keys($items);
    //dsm($nids, 'nids');
    $prices = self::getPrices($nids);
    //dsm($prices);
    return self::calculatePrice($items, $prices);
  }

  /*
   * format a single price value
   * @param array price
   *   price = array('amount' => amount, 'currency' => currency);
   */
  static public function formatPrice ($price) {
    if (empty($price['amount'])) {
      return t('free');
    }
    
    if (module_exists('currency')) {
      return rbcurrency_correct_value($price['amount']);
    } elseif (!empty($price['amount'])) {
      return $price['amount'].' '.t($price['currency']);
    }
  }

  static public function getTitleWithPrice ($node, $pricefield_name = 'field_price', $price = NULL) {
    if(empty($price)) {
      $price = $node->{$pricefield_name}[0];
    }

    if (empty($pricefield_name)) {
      $pricefield_name = 'field_price';
    }
    return $node->title.' ('.self::formatPrice($price).')';
  }
  
}
