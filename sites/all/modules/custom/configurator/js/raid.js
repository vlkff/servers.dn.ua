/*
 * @file
 * Contains JS code to handle 'Add to Cart' links using AJAX.
 */
Drupal.behaviors.configuratorRAID = function (context) {
  function IsNumeric(input)
  {
    return (input - 0) == input && input.length > 0;
  }

  /**
   * AJAX request for adding item to cart or removing item from cart.
   */
  function ajaxRequest(nid, path, onSuccess) {
    var result;
    $.ajax({
      url: Drupal.settings.basePath + path,
      data: {'nid' : nid},
      dataType: 'json',
      async: false,
      type: "POST",
      success: function(data) {
        onSuccess(nid, data);
      },
      error: function(XMLHttpRequest) {
        alert(Drupal.t("AJAX request error: !status !statusText.",
          {'!status' : XMLHttpRequest.status, '!statusText' : XMLHttpRequest.statusText}
        ));
      }
    });
    return result;
  }
  
  function optionSuccess(nid, data) {
    if (data.nid != 0) {
      // update checkbox title
      $('#edit-raid-raid-nid-raid-option-checkbox').removeAttr('checked');
      $('#edit-raid-raid-nid-raid-option-checkbox-wrapper .description').text(data.title);
      $('#edit-raid-raid-nid-raid-option-nid').val(data.nid);

      // get raid node/fix field
      ajaxRequest(nid, 'configurator/raid/get-fixed-js', optionFixedSuccess);
    } else {
      // hide checkbox if no data avaliable
      $('#edit-raid-raid-nid-raid-option-checkbox-wrapper').addClass('hide');
      // empty field from data nid
      $('#edit-raid-raid-nid-raid-option-nid').val('');
    }
  }
  
  function optionFixedSuccess(nid, data) {
    if (data) {
        // set checkbox and disable
        $('#edit-raid-raid-nid-raid-option-checkbox').attr('disabled', 'disabled');
        $('#edit-raid-raid-nid-raid-option-checkbox').attr('checked', 'checked');

      } else {
         $('#edit-raid-raid-nid-raid-option-checkbox').removeAttr('disabled');
      }
      $('#edit-raid-raid-nid-raid-option-checkbox-wrapper').removeClass('hide');
  }

  function updateOptionControls(raid_nid) {
    if (raid_nid == undefined || raid_nid == 0 || !IsNumeric(raid_nid)) {
      // hide checkbox if no option avaliable
      $('#edit-raid-raid-nid-raid-option-checkbox-wrapper').addClass('hide');
      // empty field from option nid
      $('#edit-raid-raid-nid-raid-option-nid').val('');
      return;
    }
    ajaxRequest(raid_nid, 'configurator/raid/get-option-js', optionSuccess);
  }

  // update a raid options form part on page init
  updateOptionControls($('#edit-raid-raid-nid-raid-nid').val());

  // update a raid option form part on shange selected raid controlled
  $('#edit-raid-raid-nid-raid-nid').change(function() {
    updateOptionControls($('#edit-raid-raid-nid-raid-nid').val());
});
}
