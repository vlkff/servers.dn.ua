Drupal.behaviors.configuratorPrice = function (context) {
  
  /**
   * AJAX request with parameters.
   */
  function ajaxRequest(params, path) {
    var result;
    $.ajax({
      url: Drupal.settings.basePath + path,
      data: params,
      dataType: 'json',
      async: false,
      type: "POST",
      success: function(data) {
        $('#configurator_price_value').html(data);
      },
      error: function(XMLHttpRequest) {
        alert(Drupal.t("AJAX request error: !status !statusText.",
          {'!status' : XMLHttpRequest.status, '!statusText' : XMLHttpRequest.statusText}
        ));
      }
    });
    return result;
  }
  
  function updateConfigurationPrice() {
    
    var ajaxParams = { 'nid_0' : Drupal.settings.nid, 'quantity_0' : 1 };
    var nid = 0, q = 0, qSelect = null, i = 1;
    $('#configurator-configform select:not([name*="quantity"])').
      each(function() {
      nid = parseInt($(this).val());
      if (nid) {
        q = 1;
        if ($(this).attr('name').indexOf('nid') > 0) {
          qSelect = $('#configurator-configform select[name="'+
            $(this).attr('name').replace(/nid(\])?$/, 'quantity$1')
            +'"]');
          q = qSelect.length > 0 ? parseInt(qSelect.val()) : q;
        }
        if (q == 0) return;
        ajaxParams['nid_'+i] = nid;
        ajaxParams['quantity_'+i] = q;
        i++;
      }
    });
    // raid checkboxes
    if ($('#configurator-configform input[name="raid[raid_nid][raid_option_checkbox]"]').attr('checked')) {
      ajaxParams['nid_'+i] = 
        parseInt($('#configurator-configform input[name="raid[raid_nid][raid_option_nid]"]').val());
      ajaxParams['quantity_'+i] = 1;
      i++;
    }
    var nids = [];
    $('#configurator-configform input[type="checkbox"][name^="raid"]:checked').each(function() {
      nids = $(this).attr('name').match(/\d+/g);
      if (!nids) return;
      ajaxParams['nid_'+i] = parseInt(nids[1]);
      ajaxParams['quantity_'+i] = 1;
      i++;
    });
    ajaxRequest(ajaxParams, 'configurator/calcprice-js');
  }

  $('#configurator-configform select, #configurator-configform input[type="checkbox"]').change(function() {
    updateConfigurationPrice();
  });
  updateConfigurationPrice();
}
