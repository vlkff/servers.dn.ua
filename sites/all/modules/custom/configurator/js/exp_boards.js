Drupal.behaviors.configuratorExpBoards = function (context) {

  function updateExpBoardsControls() {
    var selOpt = $('#edit-raid-raid-nid-raid-nid option:selected');
    if (Drupal.settings.exp_slots == 1 && selOpt.val() > 0) {
      $('#edit-expansion-board-nid option:first').attr('selected', 'selected');
      $('#edit-expansion-board-nid').attr('disabled', 'disabled').addClass('disabled');
      $('#expansion-board-notice').show();
    } else {
      $('#edit-expansion-board-nid').removeAttr('disabled').removeClass('disabled');
      $('#expansion-board-notice').hide();
    }
  }

  $('#edit-raid-raid-nid-raid-nid').change(function() {
    updateExpBoardsControls();
  });
}
