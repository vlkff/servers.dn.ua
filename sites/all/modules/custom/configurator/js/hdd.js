Drupal.behaviors.configuratorHDD = function (context) {
  
  /**
   * AJAX request with parameters.
   */
  function ajaxRequest(params, path) {
    var result;
    $.ajax({
      url: Drupal.settings.basePath + path,
      data: params,
      dataType: 'json',
      async: false,
      type: "POST",
      success: function(data) {
        if (!data.error) {
          for (i=0;i<3;i++) {
            $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-nid').html(data.selectsHTML[i]['nid']);
            var selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-nid option[value="'+data.selectsNidIdx[i]+'"]');
            selOpt.attr('selected', 'selected');
            if (selOpt.length == 0) {
              selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-quantity option:first');
            }
            $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-quantity').html(data.selectsHTML[i]['quantity']);
            selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-quantity option[value="'+data.selectsQIdx[i]+'"]');
            if (selOpt.length == 0) {
              selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-quantity option:first');
            }
            selOpt.attr('selected', 'selected');
          }
        }
      },
      error: function(XMLHttpRequest) {
        alert(Drupal.t("AJAX request error: !status !statusText.",
          {'!status' : XMLHttpRequest.status, '!statusText' : XMLHttpRequest.statusText}
        ));
      }
    });
    return result;
  }
  
  function makeHDDSelects() {
    $('input[id^="edit-hdd-hdd-"]').each(function (){
      var select = $('<select />');
      $(select).attr('id', $(this).attr('id'));
      $(select).attr('name', $(this).attr('name'));
      $(select).attr('style', $(this).attr('style'));
      $(select).attr('class', $(this).attr('class'));
      $(select).removeClass('hide');
      $(this).replaceWith(select);
    })
  }

  function updateHDDControls() {
    
    var ajaxParams = { 'nid' : Drupal.settings.nid };
    var i = 0;
    $('select[name^="raid"] option:selected').each(function() {
      if ($(this).val() > 0) {
        ajaxParams['raid_'+i+'_nid'] = $(this).val();
        i++;
      }
    });
    var selOpt = null;
    for (i=0;i<3;i++) {
      selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-nid option:selected');
      ajaxParams['hdd_'+i+'_nid'] = selOpt.length > 0 ? selOpt.val() : 0;
      selOpt = $('#edit-hdd-hdd-'+i+'-nid-hdd-'+i+'-quantity option:selected')
      ajaxParams['hdd_'+i+'_quantity'] = selOpt.length > 0 ? selOpt.val() : 0;
    }
    ajaxRequest(ajaxParams, 'configurator/hdd/get-selects-js');    
  }

  makeHDDSelects();
  updateHDDControls();
  $('select[name^="raid"], select[name^="hdd"]').change(function() {
    updateHDDControls();
  });  
}
