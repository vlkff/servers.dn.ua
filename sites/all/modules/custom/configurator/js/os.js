Drupal.behaviors.configuratorOS = function (context) {
  
  var selQ = $('select[name="os_quantity"]');
  var noneOpt = selQ.children('option[value="0"]').clone();
  var selectOSHTML = selQ.children('option[value="0"]').remove().end().html();
  
  function updateOSQuantity() {
    
    if ($('select[name="os_nid"]').val() == 0) {
      selQ.empty().append(noneOpt.clone());
    } else {
      var q = selQ.children('option:selected').val();
      selQ.html(selectOSHTML);
      selQ.children('option[value="'+q+'"]').attr('selected', 'selected');
    }
  }

  $('select[name^="os_nid"]').change(function() {
    updateOSQuantity();
  });
  updateOSQuantity();
}
