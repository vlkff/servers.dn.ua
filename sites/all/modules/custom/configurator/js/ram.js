Drupal.behaviors.configuratorRAM = function (context) {
  
  function updateRAMQuantity() {
    
    var el = $('select[name="ram_quantity"]');
    var nid = $('select[name="ram_nid"]').val();
    var ramType = Drupal.settings.ram[nid][0];
    var q = parseInt($(el).val());
    var ramQMin = parseInt($('select[name="cpu_quantity"]').val());
    var ramQMax = Math.min(Math.floor(Drupal.settings.max_ram[ramType]/
      Drupal.settings.ram[nid][1]), Drupal.settings.max_ram['amount']);

    if (ramType in Drupal.settings.max_ram) {
      $(el).empty();
      for (var i=ramQMin;i<=ramQMax;i++) {
        $(el).append($("<option />").val(i).text(i));
      }
      $(el).find('option[value="'+q+'"]').attr('selected', 'selected');
    }    
  }

  $('select[name^="ram_nid"], select[name^="cpu_quantity"]').change(function() {
    updateRAMQuantity();
  });
  updateRAMQuantity();
}
