<?php

/**
 * @file
 *   Implements a ConfiguratorOS class
 */


class ConfiguratorRails extends ConfiguratorFormItem {
  
  public static $basename = 'rails';

  public static $nodetype = 'rails';

  public static function getPane($form, $form_state) {

    $platform = $form_state['configurator']['node'];

    if ($platform->field_platform_rails[0]['nid']) {
      // Выбор рельс
      $pane = array(
        '#type' => 'fieldset',
        '#title' => t('Rails Settings'),
        '#weight' => 0,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      $pane[self::$basename.'_nid'] = array(
        '#type' => 'select',
        '#title' => t('Rails'),
        '#options' => self::getPaneSelectOptions(
          $platform->field_platform_rails[0]['nid']
        ),
      );
      if ($platform->field_platform_rails_fixed[0]['value']) {
        $pane[self::$basename.'_nid']['#default_value'] = $platform->field_platform_rails[0]['nid'];
        $pane[self::$basename.'_nid']['#disabled'] = TRUE;
        $pane[self::$basename.'_nid']['#attributes'] = array('class' => 'disabled');
      }
    }
    return $pane;
  }

  public static function getPaneSelectOptions($nid) {
    return array(0 => t('none')) +
      parent::getPaneSelectOptions(self::$nodetype, $nid);
  }

  public static function getSubmittedItems($form, &$form_state) {
    $return = array();
    $platform = $form_state['configurator']['node'];
    if ($platform->field_platform_rails[0]['nid']) {
      if ($platform->field_platform_rails_fixed[0]['value']) {
        $return[] = array(
            'nid' => $platform->field_platform_rails[0]['nid'],
            'quantity' => 1
        );
      } else {
        $return[] = parent::getSubmittedItems(self::$basename, $form, $form_state);
      }
    }
    return $return;
  }
}