<?php

/**
 * @file
 *   Implements a ConfiguratorOS class
 */


class ConfiguratorRAID extends ConfiguratorFormItem {
  
  public static $basename = 'raid';

  public static $nodetype = 'raid_controller';

  protected static $raid_field_name = 'field_platform_raid_controller';

  protected static $opt_field_name = 'field_raid_controller_options';

  protected static $sas_sata_tid = 62;

  protected static $external_internal_name = 2;

  public static function getPane($form, $form_state) {

    $platform = $form_state['configurator']['node'];
    $raid_fixed = (bool)$form_state['configurator']['node']->
      field_platform_raid_ctrlr_fixed[0]['value'];

    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('RAID Settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );

    // встроенные контроллеры
    $platform_controllers = self::getPlatformControllers($platform);

    foreach ($platform_controllers as $controller) {
      if ($controller->field_raid_controller_type[0]['value'] == 1) {
        $internal = TRUE;
      } elseif ($controller->field_raid_controller_type[0]['value'] == 2) {
        $internal = FALSE;
      } elseif(!empty($controller->field_raid_controller_type[0]['value'])) {
        throw new Exception('Unsupported RAID controller type value');
      }

      $pane[self::$basename.'_'.$controller->nid] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );

      $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$controller->nid] = array(
        '#type' => 'select',
        '#title' => ($internal) ? t('Internal RAID'): t('External RAID Controller'),
      );
      $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$controller->nid]['#default_value'] = $controller->nid;
      $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$controller->nid]['#options'][$controller->nid] =
        self::getTitleWithPrice($controller);
      if ($internal || $raid_fixed) {
        $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$controller->nid]['#disabled'] = TRUE;
        $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$controller->nid]['#attributes'] = array('class' => 'disabled');
      }
      
      if (!empty($controller->{self::$opt_field_name}[0]['node'])) {
        $option_node = $controller->{self::$opt_field_name}[0]['node'];
        $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$option_node->nid] = array(
          '#type' => 'checkbox',
          '#title' => $option_node->title,
        );
        if ($controller->field_raid_options_fixed[0]['value'] == 1) {
          $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$option_node->nid]['#default_value'] = 1;
          $pane[self::$basename.'_'.$controller->nid][self::$basename.'_'.$option_node->nid]['#disabled'] = TRUE;
        }
      }     
    }

    if ($platform->field_platform_exp_cards_amount[0]['value'] > 0 && !$raid_fixed) {
      drupal_add_js(drupal_get_path('module', 'configurator').'/js/raid.js');

      $pane[self::$basename.'_nid'] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      $external_controllers = self::getExternalControllers($platform_controllers);
      $pane[self::$basename.'_nid'][self::$basename.'_nid'] = array(
        '#type' => 'select',
        '#title' => t('External RAID Controller'),
        '#options' => array(0 => t('none')),
      );
      foreach ($external_controllers as $controller) {
        $pane[self::$basename.'_nid'][self::$basename.'_nid']['#options'][$controller->nid] =
          self::getTitleWithPrice($controller, NULL, array('amount' => $controller->amount, 'currency' => 'USD'));
      }

      // hidden textfield for option node nid that will place JS
      $pane[self::$basename.'_nid'][self::$basename.'_option_nid'] = array(
        '#type' => 'textfield',
        /*'#attributes' => array('class' => 'hidden'),*/
      );

      // hidden by default
      // shows if selected controller supports option
      // JS make it unchaengable if option is fixed
      // if checked JS takes by AJAX nid of option and place it to field above
      $pane[self::$basename.'_nid'][self::$basename.'_option_checkbox'] = array(
        '#type' => 'checkbox',
        '#description' => 'placeholder',
      );
    }

    return $pane;
  }

  public static function getPaneSelectOptions() {
    return parent::getPaneSelectOptions(self::$nodetype);
  }

  public static function getSubmittedItems($form, &$form_state) {
    $submitted = array();
    foreach ($form_state['values']['raid'] as $key => $item) {
      $key_exploded = explode('_', $key);
      
      if ($key == 'raid_nid' && !empty($item['raid_nid'])) {
        // submit free external controller if set
        $submitted[] = array (
          'nid' => $item['raid_nid'],
          'quantity' => 1,
        );
        // submit free external controller option if set
        if (!empty($item['raid_option_nid']) && !empty($item['raid_option_checkbox'])) {
          $submitted[] = array (
            'nid' => $item['raid_option_nid'],
            'quantity' => 1,
          );
        }
      } else {
        // submit internal controllers
        if (!empty($item[$key])) {
          $submitted[] = array (
            'nid' => $item[$key],
            'quantity' => 1,
          );
        }
        // submit internal controllers options if set
        foreach ($item as $subkey => $subvalue) {
          if ($key != $subkey && $subvalue == 1) {
            $subkey = explode('_', $subkey);
            $submitted[] = array (
              'nid' => $subkey[1],
              'quantity' => 1,
            );
          }
        }
      }
    }
    return $submitted;
  }

  protected static function getExternalControllers ($platform_controllers) {
    // если на всех впаянных контроллерах платформы доступны только SATA-порты, выводятся все контроллеры.
    // если на хоть одном впаянном контроллере платформы доступен хотя бы один SAS/SATA-порт, только контроллеры, у которых есть опции.
    // контроллер выбирается только из внешних

    // get nids we are going to load external controllers
    //dpm($platform_controllers, '$platform_controllers');

    // detect is SAS/SATA ports avaliable in INTERNAL controllers attached to the platform
    $sas_sata_avaliable = FALSE;
    foreach ($platform_controllers as $controller) {
      if ($controller->field_raid_controller_type[0]['value'] == 1) {
        $port_tids = array();
        foreach ($controller->field_raid_port_type as $item) {
          $port_tids[] = $item['value'];
        }
        if (in_array(self::$sas_sata_tid, $port_tids)) {
          $sas_sata_avaliable = TRUE;
        }
      }
    }

    $sql = 'SELECT n.nid AS nid, n.title AS title, p.field_price_amount AS amount, 
      c.field_raid_options_fixed_value AS opt_fixed,
      optn.nid AS opt_nid, optn.title AS opt_title, optp.field_price_amount AS opt_amount
      FROM
        {node} n LEFT JOIN {content_field_price} p ON n.vid = p.vid
        LEFT JOIN {content_type_raid_controller} c ON n.vid = c.vid
        LEFT JOIN {node} optn ON c.field_raid_controller_options_nid = optn.nid
        LEFT JOIN {content_field_price optp} ON c.field_raid_controller_options_nid = optp.nid
      WHERE
        n.type = "%s"
        AND c.field_raid_controller_type_value = %d';

    if ($sas_sata_avaliable) {
      $sql .= ' AND c.field_raid_controller_options_nid IS NOT NULL';
    }

    $sql .= ' ORDER BY "p.field_price_amount"';

    $result = db_query($sql, self::$nodetype, self::$external_internal_name);
    $rows = array();
    while ($row = db_fetch_object($result)) {
      $rows[$row->nid] = $row;
    }
    return $rows;
  }

  public static function getPlatformControllers($platform) {
    // get $nids array we want to load
    $nids = array();
    foreach ($platform->{self::$raid_field_name} as $item) {
      $nids[] = $item['nid'];
    }

    // load controllrs nodes
    $controllers = array();
    // ToDo: replace it so single-query code
    foreach ($nids as $nid) {
      $controllers[$nid] = node_load($nid, NULL, TRUE);
    }

    // attach to controllers nodes option nodes if its set
    foreach ($controllers as &$controller) {
      if (!empty($controller->{self::$opt_field_name}[0]['nid'])) {
        $controller->{self::$opt_field_name}[0]['node'] =
          node_load($controller->{self::$opt_field_name}[0]['nid'], NULL, TRUE);
      }
    }

    return $controllers;
  }

  public static function getOptionJS() {

    $nid = (int)check_plain($_POST['nid']);

    if (!empty($nid) && is_numeric($nid)) {

      $sql = 'SELECT n.nid AS nid, n.title AS title, p.field_price_amount AS amount, p.field_price_currency AS currency
        FROM 
          {node} n RIGHT JOIN {content_type_raid_controller} c ON n.nid = c.field_raid_controller_options_nid
          LEFT JOIN {content_field_price} p ON n.vid = p.vid
        WHERE
          c.field_raid_controller_options_nid IS NOT NULL
          AND c.nid = %d
          AND n.status = %d';

      $row = db_fetch_array(db_query($sql, $nid, 1));
      if (empty($row)) {
        $row['nid'] = 0;
      } else {
        $row['price'] = self::formatPrice($row);
      }
      
      print drupal_to_js($row);
    }
        
  }
  
  public static function getIsControllerPlatformFixed($nid) {

    $sql = '
      SELECT field_platform_raid_ctrlr_fixed_value
      FROM {content_type_platform}
      WHERE nid = "%s"';
    return (bool)db_result(db_query($sql, $nid));
  }
  
  public static function getIsControllerFixed($nid) {

    if (!is_array($nid)) {
      $nid = array($nid);
    }
    $sql = '
      SELECT SUM(field_raid_options_fixed_value) AS fixed
      FROM {content_type_raid_controller}
      WHERE nid IN (%s)';
    return (bool)db_result(db_query($sql, implode(',', $nid)));
  }

  public static function getIsControllerFixedJS() {

    $nid = (int)check_plain($_POST['nid']);

    if (!empty($nid) && is_numeric($nid)) {

      print drupal_to_js(self::getIsControllerFixed($nid));
    }
  }
  
}
