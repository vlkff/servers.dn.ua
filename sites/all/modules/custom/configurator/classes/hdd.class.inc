<?php

class ConfiguratorHDD extends ConfiguratorFormItem {
  
  public static $basename = 'hdd';
  public static $nodetype = 'hdd';
  protected static $sas_sata_tid = 62;
  protected static $hdd_types_tids = array(
    'sata' => 81,
    'sas' => 82,
  );

  public static function getPane($form, $form_state) {

    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('HDD Settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    
    $pane[self::$basename.'_0_nid'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $pane[self::$basename.'_0_nid'][self::$basename.'_0_nid'] = array(
      '#type' => 'textfield',
      '#title' => t('HDD'),
      '#attributes' => array('class' => 'hide'),
    );
    $pane[self::$basename.'_0_nid'][self::$basename.'_0_quantity'] = array(
      '#type' => 'textfield',
      '#title' => t('HDD Count'),
      '#attributes' => array('class' => 'hide'),
    );
    
    $pane[self::$basename.'_1_nid'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $pane[self::$basename.'_1_nid'][self::$basename.'_1_nid'] = array(
      '#type' => 'textfield',
      '#title' => t('HDD'),
      '#attributes' => array('class' => 'hide'),
    );
    $pane[self::$basename.'_1_nid'][self::$basename.'_1_quantity'] = array(
      '#type' => 'textfield',
      '#title' => t('HDD Count'),
      '#attributes' => array('class' => 'hide'),
    );
    $pane[self::$basename.'_2_nid'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $pane[self::$basename.'_2_nid'][self::$basename.'_2_nid'] =
      $pane[self::$basename.'_1_nid'][self::$basename.'_1_nid'];
    $pane[self::$basename.'_2_nid'][self::$basename.'_2_quantity'] = 
      $pane[self::$basename.'_1_nid'][self::$basename.'_1_quantity'];

    drupal_add_js(drupal_get_path('module', 'configurator').'/js/hdd.js');
    return $pane;
    
  }

  public static function getPaneSelectOptions($nid, $raid_nids,
  $size_tids = false, $type_tids = false) {
    
    if ($type_tids) {
      $hdd_types = $type_tids;
    } else {
      $hdd_types = self::$hdd_types_tids;

      if (!ConfiguratorRAID::getIsControllerPlatformFixed($nid)) {
        // проверяем порты sas/sata для платформы, если их нет, то исключаем
        // sas из выборки
        $raid_cond = $raid_nids ? 'AND type_raid_controller.`nid` IN('.
          implode(',', $raid_nids).')' : '';
        $result = db_query('
          SELECT field_raid_port_type.`nid`
          FROM {content_field_raid_port_type} field_raid_port_type JOIN {content_type_raid_controller} type_raid_controller
          ON field_raid_port_type.`nid` = type_raid_controller.`nid`
          WHERE field_raid_port_type.`field_raid_port_type_value` = "%s" %s',
          self::$sas_sata_tid, $raid_cond);
        if (!db_result($result)) {
          unset($hdd_types['sas']);
        }
      }
    }
    
    $result = db_query('
      SELECT node.`nid` AS nid, node.`title` AS title,
        price.`field_price_amount` AS amount,
        price.`field_price_currency` AS currency
      FROM {node} node JOIN {content_type_hdd} type_hdd
      ON node.`nid` = type_hdd.`nid`
      LEFT JOIN {content_field_price} price
      ON node.`nid` = price.`nid`
      JOIN {content_field_hdd_slot_type} field_hdd_slot_type
      ON field_hdd_slot_type.`nid` = "%s" AND 
        field_hdd_slot_type.`field_hdd_slot_type_value` =
        type_hdd.`field_hdd_size_value`
      WHERE node.`type` = "%s" AND node.`status` = %d AND
        type_hdd.`field_hdd_type_value` IN (%s) %s
      ORDER BY type_hdd.`field_hdd_type_value`, price.`field_price_amount`',
      $nid, self::$nodetype, 1, implode(',', $hdd_types),
        $size_tids ? 
        'AND type_hdd.`field_hdd_size_value` IN ('.implode(',', $size_tids).')'
        :'');
    $arr = array();
    while ($node = db_fetch_object($result)) {
      $arr[$node->nid] = $node->title
            . ' ('
            . self::formatPrice(array('amount' => $node->amount,
                'currency' => $node->currency))
            . ')';
    }
    return $arr;
  }

  public static function getPaneQuantitySelectOptions($nid, $raid_nids) {
    
    $opt = array();
    $result = db_query('
      SELECT SUM(`field_hdd_slot_amount_value`)
      FROM {content_field_hdd_slot_amount}
      WHERE `nid` = "%s"', $nid);
    $max_slots = db_result($result);
    $max_ports = $max_slots;
    if ($raid_nids && !ConfiguratorRAID::getIsControllerPlatformFixed($nid)) {
      $result = db_query('
        SELECT SUM(field_raid_port_amount.`field_raid_port_amount_value`)
        FROM {content_field_raid_port_amount} field_raid_port_amount JOIN {content_type_raid_controller} type_raid_controller
        ON field_raid_port_amount.`nid` = type_raid_controller.`nid`
        WHERE type_raid_controller.`nid` IN (%s)', implode(',', $raid_nids));
      $max_ports = db_result($result);
    }
    $opt = range(1, min($max_slots, $max_ports), 1);
    $opt = array_combine($opt, $opt);
    return $opt;
  }
  
  public static function getSubmittedItems($form, &$form_state) {

    $return = array();
    for ($i=0;$i<3;$i++) { 
      if (!empty($form_state['values'][self::$basename]
          [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_nid']) && 
        !empty($form_state['values'][self::$basename]
          [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_quantity'])) {
        $return[] = array(
          'nid' => $form_state['values'][self::$basename]
            [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_nid'],
          'quantity' => $form_state['values'][self::$basename]
            [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_quantity']
        );
      }
    }
    return $return;
  }
  
  public static function validatePaneValues($form, &$form_state) {
    
    // получить тип и размер выбранных hdd
    $hdds = array(
      'nids' => array(0, 0, 0),
      'quants' => array(0, 0, 0),
      'type_tids' => array(0, 0, 0),
      'size_tids' => array(0, 0, 0),
    );
    for ($i=0;$i<3;$i++) { 
      $hdds['nids'][$i] = (int)$form_state['values'][self::$basename]
        [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_nid'];
      $hdds['quants'][$i] = (int)$form_state['values'][self::$basename]
        [self::$basename.'_'.$i.'_nid'][self::$basename.'_'.$i.'_quantity'];
      if ($hdds['nids'][$i] > 0) {
        $result = db_query('
          SELECT field_hdd_type_value AS type, field_hdd_size_value AS size
          FROM {content_type_hdd}
          WHERE `nid` = "%s"',
          $hdds['nids'][$i]
        );
        $row = db_fetch_object($result);
        if ($row) {
          $hdds['type_tids'][$i] = $row->type;
          $hdds['size_tids'][$i] = $row->size;
        }
      }
      
    }

    // проверить выбранные hdd на соответствие количеству слотов для платформы
    $max_slots = array();
    $result = db_query('
      SELECT field_hdd_slot_type.`field_hdd_slot_type_value` AS tid,
        SUM(field_hdd_slot_amount.`field_hdd_slot_amount_value`) AS amount,
        term_data.`name` AS name
      FROM {content_field_hdd_slot_type} field_hdd_slot_type JOIN {content_field_hdd_slot_amount} field_hdd_slot_amount
      ON field_hdd_slot_type.`delta` = field_hdd_slot_amount.`delta`
      AND field_hdd_slot_type.`nid` = field_hdd_slot_amount.`nid`
      JOIN {term_data} term_data ON term_data.`tid` = field_hdd_slot_type.`field_hdd_slot_type_value`
      WHERE field_hdd_slot_type.`nid` = "%s"
      GROUP BY field_hdd_slot_type.`field_hdd_slot_type_value`',
        $form_state['configurator']['node']->nid);
    while ($row = db_fetch_object($result)) {
      $max_slots[$row->tid] = $row->amount;
      $names_slots[$row->tid] = $row->name;
    }
    $slots = $max_slots;
    for ($i=0;$i<3;$i++) {
      
      if (empty($hdds['nids'][$i])) continue;
      
      // вычитаем кол-во hdd из максимума, если он определен
      if (isset($slots[$hdds['size_tids'][$i]]))  {
        $slots[$hdds['size_tids'][$i]] -= $hdds['quants'][$i];
        foreach ($slots as $tid => $amount) {
          // выход за лимит по размеру hdd
          if ($amount < 0) {
            form_set_error(self::$basename.']['.self::$basename."_{$i}_nid][".
              self::$basename."_{$i}_quantity",
              t(
                'The number of HDDs of %size size exceeds the maximum for the platform %amount.',
                array(
                  '%size' => $names_slots[$tid],
                  '%amount' => $max_slots[$tid]
                )
              )              
            );
          }
        }
      } else {
        // hdd данного размера не поддерживается платформой
        form_set_error(self::$basename.']['.self::$basename."_{$i}_nid][".
          self::$basename."_{$i}_nid",
          t('The HDD of this size is not supported by the platform.')
        );
      }
    }
    
    // проверить выбранные hdd на соответствие выбранным raid контроллерам
        
    // определить raid контроллеры
    $raid_nids = array();
    foreach ($form_state['values']['raid'] as $key => $item) {

      if ($key == 'raid_nid' && !empty($item['raid_nid'])) {
        // external controllers
        $raid_nids[] = $item['raid_nid'];
        
      } else {
        // internal controllers
        if (!empty($item[$key])) {
          $raid_nids[] = $item[$key];
        }
      }
    }
    
    $raid_nids = array_filter($raid_nids);
    
    // если один из контроллеров фиксирован, то ограничения на порты не
    // действуют
    if (ConfiguratorRAID::getIsControllerPlatformFixed($form_state['configurator']['node']->nid)) {
      return;
    }

    if ($raid_nids) {
      
      $max_types = array();

      // определить общее количество портов - максимум для типа SATA
      $result = db_query('
        SELECT SUM(`field_raid_port_amount_value`)
        FROM {content_field_raid_port_amount}
        WHERE `nid` IN (%s)', implode(',', $raid_nids));
      $max_types[self::$hdd_types_tids['sata']] = db_result($result);

      // определить количество sas/sata портов - максимум для типа SAS
      $result = db_query('
        SELECT SUM(field_raid_port_amount.`field_raid_port_amount_value`)
        FROM {content_field_raid_port_amount} field_raid_port_amount JOIN {content_field_raid_port_type} field_raid_port_type
        ON field_raid_port_amount.`delta` = field_raid_port_type.`delta`
        AND field_raid_port_amount.`nid` = field_raid_port_type.`nid`
        WHERE field_raid_port_amount.`nid` IN (%s) AND 
          field_raid_port_type.`field_raid_port_type_value` = "%s"',
        implode(',', $raid_nids), self::$sas_sata_tid);
      $max_types[self::$hdd_types_tids['sas']] = db_result($result);

      $max_types = array_filter($max_types);

      $types = $max_types;
      for ($i=0;$i<3;$i++) {

        if (empty($hdds['nids'][$i])) continue;

        // вычитаем кол-во hdd из максимума, если он определен
        if (isset($types[$hdds['type_tids'][$i]]))  {
          $types[$hdds['type_tids'][$i]] -= $hdds['quants'][$i];
          foreach ($types as $tid => $amount) {
            // выход за лимит по типу hdd
            if ($amount < 0) {
              form_set_error(self::$basename.']['.self::$basename."_{$i}_nid][".
                self::$basename."_{$i}_quantity",
                t(
                  'The number of HDDs of %type type exceeds the maximum for the current RAID configurtion %amount.',
                  array(
                    '%type' => strtoupper(
                      array_search($tid, self::$hdd_types_tids)
                    ),
                    '%amount' => $max_types[$tid]
                  )
                )              
              );
            }
          }
        } else {
          // hdd данного типа не поддерживается платформой
          form_set_error(self::$basename.']['.self::$basename."_{$i}_nid][".
            self::$basename."_{$i}_nid",
            t('The HDD of this type is not supported by the current RAID configurtion.')
          );
        }
      }
    }
  }
  
  public static function getSelectsJS() {
    $error = true;
    $selectsHTML = array_fill(0, 3, array('nid' => '', 'quantity' => ''));
    $selectsNidIdx = array_fill(0, 3, 0);
    $selectsQIdx = array_fill(0, 3, 0);
    if (isset($_POST['nid']) && is_numeric($_POST['nid'])) {

      $raid_nids = array();
      $i = 0;
      while (isset($_POST["raid_{$i}_nid"]) &&
        is_numeric($_POST["raid_{$i}_nid"]) && $_POST["raid_{$i}_nid"] > 0) {
        $raid_nids[] = (int)$_POST["raid_{$i}_nid"];
        $i++;
      }

      // вывод по умолчанию
      $hdds_def = array(
        'nids' => array(0, 0, 0),
        'quants' => array(1, 0, 0),
        'type_tids' => array(0, 0, 0),
        'size_tids' => array(0, 0, 0),
      );
      $hdd_nids = self::getPaneSelectOptions($_POST['nid'], $raid_nids);
      $hdd_quants = 
        self::getPaneQuantitySelectOptions($_POST['nid'], $raid_nids);
      $hdds_def['nids'][0] = reset(array_keys($hdd_nids));
      $result = db_query('
        SELECT field_hdd_type_value AS type, field_hdd_size_value AS size
        FROM {content_type_hdd}
        WHERE `nid` = "%s"',
        $hdds_def['nids'][0]
      );
      $row = db_fetch_object($result);
      if ($row) {
        $hdds_def['type_tids'][0] = $row->type;
        $hdds_def['size_tids'][0] = $row->size;
      }
      $hdds = $hdds_def;
      
      // получить тип и размер выбранных hdd
      for ($i=0;$i<3;$i++) {
        if (!isset($_POST["hdd_{$i}_nid"]) || empty($_POST["hdd_{$i}_nid"]))
          continue;
          
        $hdds['nids'][$i] = (int)$_POST["hdd_{$i}_nid"];
        $hdds['quants'][$i] = (int)$_POST["hdd_{$i}_quantity"];
        if ($hdds['nids'][$i] > 0) {
          $result = db_query('
            SELECT field_hdd_type_value AS type, field_hdd_size_value AS size
            FROM {content_type_hdd}
            WHERE `nid` = "%s"',
            $hdds['nids'][$i]
          );
          $row = db_fetch_object($result);
          if ($row) {
            $hdds['type_tids'][$i] = $row->type;
            $hdds['size_tids'][$i] = $row->size;
          }
        }
      }
      
      // получить ограничения по размеру слотов и типам портов
      $max_slots = array();
      $result = db_query('
        SELECT field_hdd_slot_type.`field_hdd_slot_type_value` AS tid,
          SUM(field_hdd_slot_amount.`field_hdd_slot_amount_value`) AS amount
        FROM {content_field_hdd_slot_type} field_hdd_slot_type JOIN {content_field_hdd_slot_amount} field_hdd_slot_amount
        ON field_hdd_slot_type.`delta` = field_hdd_slot_amount.`delta`
        AND field_hdd_slot_type.`nid` = field_hdd_slot_amount.`nid`
        WHERE field_hdd_slot_type.`nid` = "%s"
        GROUP BY field_hdd_slot_type.`field_hdd_slot_type_value`',
          $_POST['nid']);
      while ($row = db_fetch_object($result)) {
        $max_slots[$row->tid] = $row->amount;
      }
      $max_types = array(
        self::$hdd_types_tids['sata'] => array_sum($max_slots),
      );
      $max_types[self::$hdd_types_tids['sas']] =
        $max_types[self::$hdd_types_tids['sata']];
      $raid_fixed = ConfiguratorRAID::getIsControllerPlatformFixed($_POST['nid']);
      if (!$raid_fixed) {
        // общее количество портов - максимум для типа SATA
        $result = db_query('
          SELECT SUM(`field_raid_port_amount_value`)
          FROM {content_field_raid_port_amount}
          WHERE `nid` IN (%s)', implode(',', $raid_nids));
        $max_types[self::$hdd_types_tids['sata']] = (int)db_result($result);
        // количество sas/sata портов - максимум для типа SAS
        $result = db_query('
          SELECT SUM(field_raid_port_amount.`field_raid_port_amount_value`)
          FROM {content_field_raid_port_amount} field_raid_port_amount JOIN {content_field_raid_port_type} field_raid_port_type
          ON field_raid_port_amount.`delta` = field_raid_port_type.`delta`
          AND field_raid_port_amount.`nid` = field_raid_port_type.`nid`
          WHERE field_raid_port_amount.`nid` IN (%s) AND 
            field_raid_port_type.`field_raid_port_type_value` = "%s"',
          implode(',', $raid_nids), self::$sas_sata_tid);
        $max_types[self::$hdd_types_tids['sas']] = (int)db_result($result);
      }

      // получить ограничения для общего выбора, если данные некорректны,
      // сбросить выбор
      $slots = $max_slots;
      $types = $max_types;
      $sata_ports = $max_types[self::$hdd_types_tids['sata']] -
        $max_types[self::$hdd_types_tids['sas']];
      $max_valid = true;
      for ($i=0;$i<3;$i++) {
      
        if (empty($hdds['nids'][$i])) continue;

        // вычитаем кол-во hdd из максимума, если он определен
        if (isset($slots[$hdds['size_tids'][$i]]))  {
          $slots[$hdds['size_tids'][$i]] -= $hdds['quants'][$i];
          if ($slots[$hdds['size_tids'][$i]] < 0) {
            $max_valid = false;
            break;
          }
        } else {
          $max_valid = false;
          break;
        }
        if (!$raid_fixed) {
          if ($hdds['type_tids'][$i] == self::$hdd_types_tids['sata']) {
            $types[self::$hdd_types_tids['sata']] -= $hdds['quants'][$i];
            $sata_ports -= $hdds['quants'][$i];
            if ($sata_ports < 0) {
              $types[self::$hdd_types_tids['sas']] -= 
                min(abs($sata_ports), $hdds['quants'][$i]);
            }              
          } elseif ($hdds['type_tids'][$i] == self::$hdd_types_tids['sas']) {
            $types[self::$hdd_types_tids['sata']] -= $hdds['quants'][$i];
            $types[self::$hdd_types_tids['sas']] -= $hdds['quants'][$i];
          }
          else {
            $max_valid = false;
            break;
          }
          if ($types[self::$hdd_types_tids['sata']] < 0 ||
            $types[self::$hdd_types_tids['sas']] < 0) {
            $max_valid = false;
            break;
          }            
        }
      }
      if (!$max_valid) {
        $hdds = $hdds_def;
        $slots = $max_slots;
        $types = $max_types;
        $slots[$hdds['size_tids'][0]] -= 1;
        $types[$hdds['type_tids'][0]] -= 1;
      }
      $sas_used = $sata_ports < 0 ? abs($sata_ports) : 0;
      
      // получить свои списки для каждого селекта
      for ($i=0;$i<3;$i++) {
        $sel_slots = $slots;
        $sel_slots[$hdds['size_tids'][$i]] += $hdds['quants'][$i];
        $sel_types = $types;
        $sel_types[$hdds['type_tids'][$i]] += $hdds['quants'][$i];
        if ($hdds['type_tids'][$i] == self::$hdd_types_tids['sas']) {
          $sel_types[self::$hdd_types_tids['sata']] += $hdds['quants'][$i];
        }
        $hdd_nids = ($i == 0 ? array() : array(0 => t('none')));
        $hdd_quants = ($i == 0 ? array() : array(0 => t('none')));
        // убрать типы, на которые нельзя поменять выбор
        foreach ($sel_slots as $size_tid => $q) {
          if ($size_tid != $hdds['size_tids'][$i] && $hdds['quants'][$i] > $q) {
            unset($sel_slots[$size_tid]);
          }
        }
        foreach ($sel_types as $type_tid => $q) {
          if ($type_tid == $hdds['type_tids'][$i]) continue;
          if ($type_tid == self::$hdd_types_tids['sata'] &&
            ($q == 0 || $hdds['quants'][$i] > $q)) {
            unset($sel_types[$type_tid]);              
          } elseif ($type_tid == self::$hdd_types_tids['sas']) {
            $sas_ports = $q;
            if ($sas_used > 0) {
              $sas_ports += min($sas_used, $hdds['quants'][$i]);
            }
            if ($sas_ports == 0 || $hdds['quants'][$i] > $sas_ports) {
              unset($sel_types[$type_tid]);
            }            
          }
        }
        $sel_slots = array_filter($sel_slots);
        if ($sel_slots && $sel_types) {
          $hdd_nids += (array)self::getPaneSelectOptions($_POST['nid'],
            $raid_nids, array_keys($sel_slots), array_keys($sel_types));
          $hdd_max = min($sel_slots[$hdds['size_tids'][$i]],
            $sel_types[$hdds['type_tids'][$i]]);
          if ($hdds['nids'][$i] && $hdd_max) {
            $qvals = range(1, $hdd_max);
            $hdd_quants += array_combine($qvals, $qvals);
          }
        }
        foreach ($hdd_nids as $val => $name) {
          $selectsHTML[$i]['nid'] .= '<option value="'.$val.'">'.$name.'</option>'."\n";
        }
        foreach ($hdd_quants as $val => $name) {
          $selectsHTML[$i]['quantity'] .= '<option value="'.$val.'">'.$name.'</option>'.
            "\n";
        }
        $selectsNidIdx[$i] = $hdds['nids'][$i];
        $selectsQIdx[$i] = $hdds['quants'][$i];
      }
      $error = false;
    }
    drupal_json(compact('error', 'selectsHTML', 'selectsNidIdx', 'selectsQIdx'));
  }
  
}
