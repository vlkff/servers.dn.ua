<?php

/**
 * @file
 *   Implements a ConfiguratorOS class
 */


class ConfiguratorOS extends ConfiguratorFormItem {
  
  public static $basename = 'os';

  public static $nodetype = 'os';

  public static function getPane($form, $form_state) {

    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('OS Settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $pane[self::$basename.'_nid'] = array(
      '#type' => 'select',
      '#title' => t('Operation system (OS)'),
      '#options' => self::getPaneSelectOptions(),
    );
    $pane[self::$basename.'_quantity'] = array(
      '#type' => 'select',
      '#title' => t('OS Count'),
      '#options' => self::getPaneQuantitySelectOptions(),
    );
    
    drupal_add_js(drupal_get_path('module', 'configurator').'/js/os.js');
    return $pane;
  }

  public static function getPaneSelectOptions() {
    return array(0 => t('none')) +
      parent::getPaneSelectOptions(self::$basename);
  }

  public static function getPaneQuantitySelectOptions () {
    $opt = array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50);
    return array(0 => t('none')) + array_combine(range(1, 10) , $opt);
  }

  public static function getSubmittedItems($form, &$form_state) {
    return parent::getSubmittedItems(self::$basename, $form, $form_state);
  }
}