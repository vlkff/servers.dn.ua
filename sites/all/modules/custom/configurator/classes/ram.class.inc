<?php

class ConfiguratorRAM extends ConfiguratorFormItem {
  
  public static $basename = 'ram';

  public static $nodetype = 'ram';

  public static function getPane($form, $form_state) {
    
    // Выбор RAM
    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('RAM settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $ram = self::getPaneSelectOptions($form_state['configurator']['node']->nid);
    $pane[self::$basename.'_nid'] = array(
      '#type' => 'select',
      '#title' => t('RAM'),
      '#options' => $ram[0]
    );
    $pane[self::$basename.'_quantity'] = array(
      '#type' => 'select',
      '#title' => t('RAM Count'),
      '#options' => self::getPaneQuantitySelectOptions(
        $form_state['configurator']['node']->nid
      ),
      '#default_value' => ConfiguratorCPU::getMaxCpu(
        $form_state['configurator']['node']->nid
      )
    );
    
    $max_ram = array();
    foreach($form_state['configurator']['node']->field_platform_memory_type as
      $i => $mt) {
        $max_ram[$mt['value']] = $form_state['configurator']['node']->
          field_platform_memory_max_size[$i]['value'];
    }
    $max_ram['amount'] = $form_state['configurator']['node']->
      field_platform_ram_slot_amount[0]['value'];
    drupal_add_js(drupal_get_path('module', 'configurator').'/js/ram.js');
    drupal_add_js(array('ram' => $ram[1], 'max_ram' => $max_ram), 'setting');
    return $pane;
  }

  public static function getPaneSelectOptions($nid) {
    
    $result = db_query('
      SELECT node.`nid` AS nid, node.`title` AS title,
        type_ram.`field_ram_module_type_value` AS type, type_ram.`field_ram_module_size_value` AS size,
        price.`field_price_amount` AS amount,
        price.`field_price_currency` AS currency
      FROM {node} node JOIN {content_type_ram} type_ram
      ON node.`nid` = type_ram.`nid`
      LEFT JOIN {content_field_price} price
      ON node.`nid` = price.`nid`
      JOIN {content_field_platform_memory_type} platform_memory_type
      ON platform_memory_type.`nid` = "%s" AND
        platform_memory_type.`field_platform_memory_type_value` = type_ram.`field_ram_module_type_value`
      JOIN {content_field_platform_memory_max_module} platform_memory_max_module
      ON platform_memory_max_module.`nid` = "%s" AND
        platform_memory_max_module.`delta` = platform_memory_type.`delta` AND
        platform_memory_max_module.`field_platform_memory_max_module_value` >= type_ram.`field_ram_module_size_value`
      WHERE node.`type` = "%s" AND node.`status` = %d
      ORDER BY price.`field_price_amount`', $nid, $nid, self::$nodetype, 1);
    $arr0 = array();
    $arr1 = array();
    while ($node = db_fetch_object($result)) {
      $arr0[$node->nid] = $node->title
            . ' ('
            . self::formatPrice(array('amount' => $node->amount,
                'currency' => $node->currency))
            . ')';
      $arr1[$node->nid] = array($node->type, $node->size);
    }
    return array($arr0, $arr1);
  }
  
  public static function getPaneQuantitySelectOptions($nid) {
    
    $opt = array();
    $result = db_query('
      SELECT`field_platform_ram_slot_amount_value`
      FROM {content_type_platform}
      WHERE `nid` = "%s"', $nid);
    $opt = range(1, db_result($result), 1);
    $opt = array_combine($opt, $opt);
    return $opt;
  }

  public static function getSubmittedItems($form, &$form_state) {
    return parent::getSubmittedItems(self::$basename, $form, $form_state);
  }
  
  public static function validatePaneValues($form, &$form_state) {
    
    if ($form_state['values'][self::$basename.'_quantity'] <
      $form_state['values']['cpu_quantity'])
      form_set_error(self::$basename.'_quantity',
        t('The number of memory modules should be greater than or equal to the number of processors.')
      );
    $max_ram = $form_state['configurator']['node']->field_platform_ram_slot_amount[0]['value'];
    if ($form_state['values'][self::$basename.'_quantity'] > $max_ram) {
      form_set_error(self::$basename.'_quantity',
        t(
          'The number of memory modules exceeds the allowable values for this platform %amount.',
          array('%amount' => $max_ram)          
        )
      );
    }
    $result = db_query('
      SELECT term_data.`name` AS type, platform_memory_max_size.`field_platform_memory_max_size_value` AS max_size, type_ram.`field_ram_module_size_value` AS selected_size
      FROM {content_field_platform_memory_type} platform_memory_type
      JOIN {content_field_platform_memory_max_size} platform_memory_max_size 
      ON platform_memory_type.`nid` = platform_memory_max_size.`nid` AND
      platform_memory_type.`delta` = platform_memory_max_size.`delta`
      JOIN {content_type_ram} type_ram ON
      type_ram.`field_ram_module_type_value` = platform_memory_type.`field_platform_memory_type_value`
      JOIN {term_data} term_data
      ON term_data.`tid` = platform_memory_type.`field_platform_memory_type_value`
      WHERE platform_memory_type.`nid` = "%s" AND type_ram.`nid` = "%s"',
      $form_state['configurator']['node']->nid,
      $form_state['values'][self::$basename.'_nid']
    );
    $ram = db_fetch_object($result);
    if ($ram && $ram->max_size < $ram->selected_size*
      $form_state['values'][self::$basename.'_quantity']) {
        form_set_error(self::$basename.'_quantity',
          t(
            'The number of memory modules of %type type exceeds the allowable amount of memory for this platform %amount Gb.',
            array('%type' => $ram->type, '%amount' => $ram->max_size)
          )
        );
    }
  }
}