<?php

/**
 * @file
 *   Implements a ConfiguratorOS class
 */


class ConfiguratorExpBoards extends ConfiguratorFormItem {
  
  public static $basename = 'expansion_board';

  public static $nodetype = 'expansion_board';

  public static function getPane($form, $form_state) {

    $external_raid = false;
    $platform = $form_state['configurator']['node'];
    $platform_controllers = ConfiguratorRAID::getPlatformControllers($platform);
    foreach ($platform_controllers as $controller) {
      if ($controller->field_raid_controller_type[0]['value'] == 2) {
        $external_raid = true;
      }
    }
    if (($platform->field_platform_exp_cards_amount[0]['value'] == 1 && !$external_raid) 
      || $platform->field_platform_exp_cards_amount[0]['value'] > 1) {
      $pane = array(
        '#type' => 'fieldset',
        '#title' => t('Expansion board settings'),
        '#weight' => 0,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      $pane[self::$basename.'_notice'] = array(
        '#value' => '<div id="expansion-board-notice" style="display:none">'
          .t('Slot of the platform is occupied by an external RAID controller.')
          .'</div>',
      );
      $pane[self::$basename.'_nid'] = array(
        '#type' => 'select',
        '#title' => t('Expansion board'),
        '#options' => self::getPaneSelectOptions(),
      );

      drupal_add_js(drupal_get_path('module', 'configurator').'/js/exp_boards.js');
      drupal_add_js(
        array('exp_slots' => $platform->field_platform_exp_cards_amount[0]['value']),
        'setting'
      );
    }
    return $pane;
  }

  public static function getPaneSelectOptions() {
    return array(0 => t('none')) +
      parent::getPaneSelectOptions(self::$nodetype);
  }

  public static function getSubmittedItems($form, &$form_state) {
    return parent::getSubmittedItems(self::$basename, $form, $form_state);
  }
  
  public static function validatePaneValues($form, &$form_state) {
 
    if (!$form_state['values'][self::$basename.'_nid']) {
        return;
    }
    $platform = $form_state['configurator']['node'];
    if ($platform->field_platform_exp_cards_amount[0]['value'] == 0) {
      form_set_error(self::$basename.'_nid',
          t('This platform has no expansion slots.')
        );
    }
    $external_raid = false;
    $platform_controllers = ConfiguratorRAID::getPlatformControllers($platform);
    foreach ($platform_controllers as $controller) {
      if ($controller->field_raid_controller_type[0]['value'] == 2) {
        $external_raid = true;
      }
    }
    if (!$external_raid) {
      foreach ($form_state['values']['raid'] as $key => $item) {
        if ($key == 'raid_nid' && !empty($item['raid_nid'])) {
          $external_raid = true;
        }
      }
    }
    if ($external_raid &&
      $platform->field_platform_exp_cards_amount[0]['value'] == 1) {
        form_set_error(self::$basename.'_nid',
          t('Slot of the platform is occupied by an external RAID controller.')
        );
    }
  }
}