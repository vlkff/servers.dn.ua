<?php

/**
 * @file
 *   Implements a ConfiguratorOS class
 */


class ConfiguratorUPS extends ConfiguratorFormItem {
  
  public static $basename = 'ups';

  public static $nodetype = 'ups';

  public static function getPane($form, $form_state) {
    // Выбор UPS
    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('UPS settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $pane[self::$basename.'_nid'] = array(
      '#type' => 'select',
      '#title' => t('UPS'),
      '#options' => self::getPaneSelectOptions(),
    );
    return $pane;
  }

  public static function getPaneSelectOptions() {
    return array(0 => t('none')) +
      parent::getPaneSelectOptions(self::$nodetype);
  }

  public static function getSubmittedItems($form, &$form_state) {
    return parent::getSubmittedItems(self::$basename, $form, $form_state);
  }
}