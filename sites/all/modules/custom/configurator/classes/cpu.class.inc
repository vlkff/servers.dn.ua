<?php

class ConfiguratorCPU extends ConfiguratorFormItem {
  
  public static $basename = 'cpu';

  public static $nodetype = 'cpu';

  public static function getPane($form, $form_state) {
      
    // Выбор CPU
    $pane = array(
      '#type' => 'fieldset',
      '#title' => t('CPU settings'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $pane[self::$basename.'_nid'] = array(
      '#type' => 'select',
      '#title' => t('CPU'),
      '#options' => self::getPaneSelectOptions(
        $form_state['configurator']['node']->nid),
    );
    $quant = self::getPaneQuantitySelectOptions(
      $form_state['configurator']['node']->nid
    );
    if ($quant) {
      $pane[self::$basename.'_quantity'] = array(
        '#type' => 'select',
        '#title' => t('CPU Count'),
        '#options' => $quant,
      );
    }
    return $pane;
  }

  public static function getPaneSelectOptions($nid) {
    
    $result = db_query('
      SELECT node.`nid` AS nid, node.`title` AS title,
        price.`field_price_amount` AS amount,
        price.`field_price_currency` AS currency
      FROM {node} node JOIN {content_type_cpu} type_cpu
      ON node.`nid` = type_cpu.`nid`
      LEFT JOIN {content_field_price} price
      ON node.`nid` = price.`nid`
      WHERE node.`type` = "%s" AND node.`status` = %d AND
      type_cpu.`field_cpu_series_value` IN 
        (SELECT `field_platform_cpu_series_value`
        FROM content_field_platform_cpu_series
        WHERE `nid` = "%s")
      ORDER BY price.`field_price_amount`', self::$nodetype, 1, $nid);
    $arr = array();
    while ($node = db_fetch_object($result)) {
      $arr[$node->nid] = $node->title
            . ' ('
            . self::formatPrice(array('amount' => $node->amount,
                'currency' => $node->currency))
            . ')';
    }
    return $arr;    
  }
  
  public static function getPaneQuantitySelectOptions($nid) {
    
    $opt = array();
    $max_cpu = self::getMaxCpu($nid);
    if ($max_cpu > 1) {
      $opt = range($max_cpu, 1, -1);
      $opt = array_combine($opt, $opt);
    }  
    return $opt;
  }
  
  public static function getMaxCpu($nid) {
    
    $result = db_query('
      SELECT MAX(`field_cpu_amount_value`)
      FROM {term_fields_term}
      WHERE `tid` IN
        (SELECT `field_platform_cpu_series_value`
        FROM content_field_platform_cpu_series
        WHERE `nid` = "%s")', $nid);
    $max_cpu = db_result($result);
    return $max_cpu;
  }

  public static function getSubmittedItems($form, &$form_state) {
    return parent::getSubmittedItems(self::$basename, $form, $form_state);
  }
  
  public static function validatePaneValues($form, &$form_state) {
    $result = db_query('
      SELECT `field_cpu_amount_value`
      FROM {term_fields_term}
      WHERE `tid` = 
        (SELECT `field_cpu_series_value`
        FROM {content_type_cpu}
        WHERE `nid` = "%s")', $form_state['values'][self::$basename.'_nid']);
    $max_cpu = db_result($result);
    if ($form_state['values'][self::$basename.'_quantity'] > $max_cpu) {
      form_set_error(self::$basename.'_quantity',
        t(
          'The number of processors exceeds the allowable values for this series %amount.',
          array('%amount' => $max_cpu)
        )
      );
    }
  }
}