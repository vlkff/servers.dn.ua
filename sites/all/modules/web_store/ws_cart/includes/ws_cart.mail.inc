<?php
/*   function build Mail Headers and Mail Body
 *   &$multipart, &$headers - return values
 *   $to - reviewer email
 *   $from - from-field
 *   $sbj - message subject
 *   $msg - message - plain text or html
 *   $use_html - set true if $msg is html
 *   $encoding - mail encoding, UTF-8 as default
 *   $pathes - строка с путями к файлам через запятую
 */
function ws_cart_mail_prepare_data(&$multipart, &$headers, $to, $from = NULL, $sbj=NULL, $msg = NULL, $use_html = false, $encoding = 'utf-8', $pathes = NULL){
      //разделитель
      $b = "--".md5(uniqid(time()));

      //если нужно явно указать отправителя
      if( $from != NULL ) {
          $headers .= "From: $from\r\n";
      }
      if( $to != NULL ) {
          $headers .= "To: $to\r\n";
      }

      if( $sbj != NULL ) {
          $headers .= "Subject: $sbj\r\n";
      }

      //если присоеденинен файл
    if( $pathes != NULL ) {
        $use_mime = true;
        $tmp_pathes = $pathes;

        while(substr_count($tmp_pathes, ',')) {
             $pth = substr($tmp_pathes, 0, strpos($tmp_pathes, ","));
                 //echo "файл:".$pth.'<br>';
             $tmp_pathes = str_replace($pth.', ', '', $tmp_pathes);
                 //echo "строка с файлами:".$tmp_pathes.'<br>';

             $f = fopen($pth,"r");
             if( !$f )return false;
             $file = fread($f, filesize($pth));
             fclose($f);


             //начинаем писать аргумент для DATA с разделителя
             //пишем часть с файлом
             $message_part = "--$b\n";
             $message_part .= "Content-Type: application/octet-stream\n";
             $message_part .= "Content-Transfer-Encoding: base64\n";
             $message_part .= "Content-Disposition: attachment; filename = \"".$pth."\"\n\n";
             $message_part .= chunk_split(base64_encode($file))."\n";

        }

             $pth = $tmp_pathes;
             $f = fopen($pth,"r");
             if( !$f )return false;
             $file = fread($f, filesize($pth));
             fclose($f);


          //начинаем писать аргумент для DATA с разделителя
          //пишем часть с файлом
          $message_part .= "--$b\n";
          $message_part .= "Content-Type: application/octet-stream\n";
          $message_part .= "Content-Transfer-Encoding: base64\n";
          $message_part .= "Content-Disposition: attachment; filename = \"".$pth."\"\n\n";
          $message_part .= chunk_split(base64_encode($file))."\n";
      }// end if (!empty($pathes))

      if (!($use_html === false)){
           $use_mime = true;
          //устанавливаем кодировку

          $multipart .= "--$b\n";
          $multipart .= "Content-Type: text/html; charset=$encoding\n";
          $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
          //вставляем текст
          $multipart .= "$msg\n\n";
      }else{
          if ($use_mime) {
              $multipart .= "--$b\n";
              $multipart .= "Content-Type: text/plain\n\n";
              $muttipart .= "Content-Transfer-Encoding: 8bit";
              $muttipart .= "Content-Disposition: inline";
              $multipart .= "$msg\n\n";
          } else $multipart .= "$msg\n\n";

      }

      // добавляем к текстовой части кодированные приложения, если есть
      if(!empty($message_part)){
      	$multipart .= $message_part;
      }

      // Пишем заголовки и Дописываем в конец разделитель, если письмо MIME-типа
    if($use_mime){
    	$headers .= "MIME-Version: 1.0\r\n";
    	$headers .= "Content-Type: multipart/mixed; boundary =\"$b\"\r\n";
    	// Дописываем в конец разделитель
    	$multipart .= "--$b--\r\n";
    }

    return true;
}


function ws_cart_mail_localserver($to, $from = NULL , $sbj = NULL , $msg, $use_html = false, $encoding = 'utf-8', $pathes = NULL){
    $multipart = '';
    $headers = '';
    $gooddata = ws_cart_mail_prepare_data($multipart, $headers, $to, $from , $sbj, $msg, $use_html, $encoding , $pathes);
    if($gooddata === false){
		return false;
	}

 	if (!mail($to, $sbj, $multipart, $headers)) return false;
 	return true;
}